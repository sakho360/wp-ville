<?php
/**
 * Image section
 *
 * @package zerif-lite
 */

?>
	<div class="section-image">
		<img class="section-image-img" src="<?php echo get_template_directory_uri(); ?>/images/club-nautique.jpg">
	</div>
